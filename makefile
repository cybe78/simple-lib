all: main.cpp libSayHello.a
		g++ -o test main.cpp -lSayHello -L.
all-shared: main.cpp libSayHello.so
		g++ -o test main.cpp -lSayHello -L.
libSayHello.a: SayHello.o
		ar rvs libSayHello.a SayHello.o
libSayHello.so: SayHello.o
		g++ -o libSayHello.so -shared SayHello.o
SayHello.o: SayHello.cpp
		g++ -c -fPIC SayHello.cpp
clean:
		rm -f *.o *.a *.so test
